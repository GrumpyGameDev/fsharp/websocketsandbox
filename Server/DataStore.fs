namespace Server

open System
open System.Net.WebSockets

module internal DataStore =
    let private sessions : Ref<Map<Guid,SessionData>> = ref Map.empty
    let private avatars : Ref<Map<Guid, Avatar>> = ref Map.empty

    let internal updateSessions (updater:Map<Guid,SessionData>->Map<Guid,SessionData>) : unit =
        Utility.withLock
            (fun () ->
                sessions := sessions.Value |> updater)

    let internal createSession (socket:WebSocket) (sessionId:Guid) : unit =
        updateSessions (Map.add sessionId (SessionData.create DateTimeOffset.Now socket))

    let internal updateSession (sessionId:Guid) : unit =
        sessions.Value
        |> Map.tryFind sessionId
        |> Option.iter 
            (Map.add sessionId >> updateSessions)

    let internal updateAvatars (updater:Map<Guid,Avatar>->Map<Guid,Avatar>) : unit =
        Utility.withLock
            (fun () ->
                avatars := avatars.Value |> updater)

    let internal getAvatar (sessionId:Guid) : Avatar option =
        avatars.Value
        |> Map.tryFind sessionId

    let internal createAvatar (sessionId:Guid) : unit =
        updateAvatars (Map.add sessionId (Avatar.create()))
