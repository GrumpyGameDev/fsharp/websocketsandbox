namespace Server

open System
open Thoth.Json.Net

type InitResult =
    {
        resultType: ResultType
        sessionId: Guid
    }
    static member Encode (result:InitResult) : JsonValue =
        Encode.object
            [
                "resultType", ResultType.Encode result.resultType
                "sessionId", Encode.guid result.sessionId
            ]

module InitResult =
    let create (sessionId: Guid) =
        {
            resultType = Init
            sessionId = sessionId
        }