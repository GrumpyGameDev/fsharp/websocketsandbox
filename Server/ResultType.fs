namespace Server

open Thoth.Json.Net

type ResultType =
    | Init 
    | Status
    | InvalidSession
    override x.ToString() =
        match x with 
        | Init -> "init"
        | Status -> "status"
        | InvalidSession -> "invalid-session"
    static member Encode (resultType:ResultType) : JsonValue =
        Encode.string (resultType.ToString())
