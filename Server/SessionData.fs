namespace Server

open System
open System.Net.WebSockets

type SessionData =
    {
        lastActive: DateTimeOffset
        socket: WebSocket
    }

module internal SessionData =
    let internal create (lastActive:DateTimeOffset) (socket:WebSocket) : SessionData =
        {
            lastActive = lastActive
            socket = socket
        }

    let internal update (lastActive:DateTimeOffset) (data:SessionData) : SessionData =
        {data with lastActive = lastActive}