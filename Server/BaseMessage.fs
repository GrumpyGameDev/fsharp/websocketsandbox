namespace Server

open Thoth.Json.Net

type BaseMessage =
    {
        auth: string
        messageType: MessageType
    }
    static member Decoder : Decoder<BaseMessage> =
        Decode.object
            (fun get ->
                {
                    auth = get.Required.Field "auth" Decode.string
                    messageType = get.Required.Field "messageType" MessageType.Decoder
                })