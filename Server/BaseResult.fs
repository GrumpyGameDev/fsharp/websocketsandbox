namespace Server

open Thoth.Json.Net

type BaseResult =
    {
        resultType: ResultType
    }
    static member Encode (result:BaseResult) : JsonValue =
        Encode.object
            [
                "resultType", ResultType.Encode result.resultType
            ]

module internal BaseResult =
    let internal invalidSession () : BaseResult =
        {
            resultType = InvalidSession
        }

