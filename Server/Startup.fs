namespace Server

open System
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open System.Threading
open System.Net.WebSockets
open Thoth.Json.Net

type Startup() =

    member this.ConfigureServices(services: IServiceCollection) =
        ()

    member this.Configure(app: IApplicationBuilder, env: IWebHostEnvironment) =
        if env.IsDevelopment() then
            app.UseDeveloperExceptionPage() |> ignore

        app.UseRouting() |> ignore

        app.UseDefaultFiles() |> ignore

        app.UseStaticFiles() |> ignore

        app.UseFileServer(enableDirectoryBrowsing = true) |> ignore

        app.UseWebSockets() |> ignore

        app.Map(PathString("/ws"), 
            fun builder -> 
                builder.Use(fun next ->
                    RequestDelegate
                        (fun context -> 
                            if context.WebSockets.IsWebSocketRequest then
                                let socket = context.WebSockets.AcceptWebSocketAsync().Result
                                Server.serve(socket)
                            else
                                next.Invoke(context))) |> ignore) |> ignore
            