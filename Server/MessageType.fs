namespace Server

open Thoth.Json.Net

type MessageType =
    | Init
    | Status
    static member Decoder : Decoder<MessageType> = 
        fun path value ->
            match value.ToString() with
            | "init" -> Ok Init
            | "status" -> Ok Status
            | _ -> (path, BadPrimitive("a MessageType", value)) |> Error
            

