var utility = {
    "toDMS":function(radians){
        radians = Math.atan2(Math.sin(radians), Math.cos(radians));
        if(radians<0){
            radians = radians + Math.PI * 2;
        }

        let degrees = radians * 180 / Math.PI;
        let wholeDegrees = Math.floor(degrees);

        let minutes = (degrees - wholeDegrees) * 60;
        let wholeMinutes = Math.floor(minutes);

        let seconds = (minutes - wholeMinutes) * 60;
        let wholeSeconds = Math.floor(seconds);
        
        return {
            "degrees": wholeDegrees,
            "minutes": wholeMinutes,
            "seconds": wholeSeconds
        };
    },
    "fromDMS":function(degrees, minutes, seconds){
        return ((((seconds/60)+minutes)/60)+degrees) * Math.PI / 180;
    },
    "toDMSString":function(radians){
        let dms = this.toDMS(radians);
        return dms.degrees+"&deg;"+dms.minutes+"\'"+dms.seconds+"\"";
    }
}