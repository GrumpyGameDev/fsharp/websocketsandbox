var controller= {
    "data": {},
    "state":"start",
    "states":{
        "start" : startController,
        "loading": loadingController,
        "atSea": atSeaController
    },
    "handleMessage": function(message){
        if(message.resultType=="init"){
            this.sessionId = message.sessionId;
            this.state = "loading";
            this.refresh();
            this.client.send({
                "auth": this.sessionId,
                "messageType": "status"
            })
        }else if(message.resultType=="status"){
            this.state = "atSea";
            this.data.avatar = message.avatar;
            this.refresh();
        }
    },
    "refresh":function(){
        this.states[this.state].refresh();
    },
    "doCommand":function(command, parameters){
        this.states[this.state].doCommand(command,parameters);
        this.refresh();
    },
    "start":function(client){
        this.client = client;
        for(var k in this.states){
            this.states[k].client = this.client;
            this.states[k].controller = this
        }
        this.refresh();
    }
}