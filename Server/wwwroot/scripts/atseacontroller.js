var atSeaController = {
    "doCommand":function(command, parameters){
    },
    "refresh":function(){
        var view = document.getElementById("view");

        var content = "";
    
        content += "<h3>Status: At Sea!</h3>";

        let avatar = this.controller.data.avatar;

        content += "<p>Location: ("+avatar.x+","+avatar.y+")</p>"
        content += "<p>Heading: "+utility.toDMSString(avatar.heading)+"<button type=\"button\" class=\"btn btn-link\" onclick=\"controller.doCommand('changeHeading')\">(change)</button></p>"
        content += "<p>Speed: "+avatar.speed+"<button type=\"button\" class=\"btn btn-link\" onclick=\"controller.doCommand('changeSpeed')\">(change)</button></p>"

        content += "<p><button type=\"button\" class=\"btn btn-link\" onclick=\"controller.doCommand('move')\">Sail!</button></p>"
    
        view.innerHTML = content;
    }
};