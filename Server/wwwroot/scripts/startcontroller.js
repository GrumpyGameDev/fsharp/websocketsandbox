var startController = {
    "doCommand": function (command, parameters) {
        if (command == "startGame") {
            let startMessage = {
                "auth": "",
                "messageType": "init"
            };
            this.client.send(startMessage);
        }
    },
    "refresh": function () {
        var view = document.getElementById("view");

        var content = "";
        content += "<h1>Seafarers of Splorr!!</h1>"

        content += "<p><button type=\"button\" class=\"btn btn-link\" onclick=\"controller.doCommand('startGame')\">Start!</button></p>";
        content += "<p><button type=\"button\" class=\"btn btn-link\" onclick=\"controller.doCommand('instructions')\">Instruct!</button></p>";
        content += "<p><button type=\"button\" class=\"btn btn-link\" onclick=\"controller.doCommand('about')\">About!</button></p>";

        view.innerHTML = content;
    }
};