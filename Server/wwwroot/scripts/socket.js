var client = {
    "send":function(message){
        this.socket.send(JSON.stringify(message));
    },
    "init":function(){
        var webSocketProtocol = location.protocol == "https:" ? "wss:" : "ws:";
        var webSocketURI = webSocketProtocol + "//" + location.host + "/ws";
        
        this.socket = new WebSocket(webSocketURI);
        
        this.socket.onopen = function () {
            console.log("Connected.");
        };
        
        this.socket.onclose = function (event) {
            if (event.wasClean) {
                console.log('Disconnected.');
            } else {
                console.log('Connection lost.'); // for example if server processes is killed
            }
            console.log('Code: ' + event.code + '. Reason: ' + event.reason);
        };
        
        this.socket.onmessage = function (event) {
            console.log("Data received: " + event.data);
            controller.handleMessage(JSON.parse(event.data));
        };
        
        this.socket.onerror = function (error) {
            console.log("Error: " + error.message);
        };
        
    },
}
client.init();

