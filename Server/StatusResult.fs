namespace Server

open Thoth.Json.Net

type StatusResult =
    {
        resultType: ResultType
        avatar: Avatar
    }
    static member Encode (result:StatusResult) : JsonValue =
        Encode.object
            [
                "resultType", ResultType.Encode result.resultType
                "avatar", Avatar.Encode result.avatar
            ]

module internal StatusResult =
    let create (avatar:Avatar) : StatusResult =
        {
            resultType = Status
            avatar = avatar
        }