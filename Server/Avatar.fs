namespace Server

open Thoth.Json.Net

type Avatar = 
    {
        x: float
        y: float
        heading: float
        speed: float
    }
    static member Encode (result:Avatar) : JsonValue =
        Encode.object
            [
                "x", Encode.float result.x
                "y", Encode.float result.y
                "heading", Encode.float result.heading
                "speed", Encode.float result.speed
            ]

module internal Avatar =
    let internal create() =
        {
            x = 0.0
            y = 0.0
            heading = 0.0
            speed = 1.0
        }