namespace Server

open System
open System.Net.WebSockets

module internal InitHandler =
    let internal handle (auth:string) (message:string) (socket:WebSocket) : unit =
        let sessionId = Guid.NewGuid()

        DataStore.createSession socket sessionId

        DataStore.createAvatar sessionId

        InitResult.create sessionId
        |> InitResult.Encode
        |> Utility.sendMessageToClient socket
