namespace Server

open System
open System.Threading
open System.Net.WebSockets
open Thoth.Json.Net


module internal Utility =
    let private monitor = Object()
    let internal withLock (operation:unit->'T) : 'T =
        Monitor.Enter monitor
        try
            operation()
        finally
            Monitor.Exit monitor

    let internal sendMessageToClient (socket:WebSocket) (message:JsonValue) : unit =
        let bytes = System.Text.Encoding.UTF8.GetBytes(message |> Encode.toString 0)
        socket.SendAsync(ArraySegment<byte>(bytes), WebSocketMessageType.Text, true, CancellationToken.None).Wait()

