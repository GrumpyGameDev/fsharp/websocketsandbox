namespace Server

open System
open System.Net.WebSockets

module internal StatusHandler =
    let internal handle (auth:string) (_:string) (socket:WebSocket) : unit =
        match Guid.TryParse auth with
        | false, _ ->
            BaseResult.invalidSession()
            |> BaseResult.Encode
        | true, sessionId ->
            DataStore.getAvatar sessionId
            |> Option.map (StatusResult.create >> StatusResult.Encode)
            |> Option.defaultValue (BaseResult.invalidSession() |> BaseResult.Encode)
        |> Utility.sendMessageToClient socket
