namespace Server

open System
open System.Threading
open System.Net.WebSockets
open Thoth.Json.Net

module internal Server =

    let private handlerTable: Map<MessageType, string -> string -> WebSocket -> unit> =
        [
            MessageType.Init, InitHandler.handle
            MessageType.Status, StatusHandler.handle
        ]
        |> Map.ofList

    let rec private keepServing (buffer:byte array) (socket:WebSocket) : Tasks.Task =
        let result = socket.ReceiveAsync(ArraySegment<byte>(buffer), CancellationToken.None).Result
        if result.CloseStatus.HasValue then
            socket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None)
        else
            let message = System.Text.Encoding.UTF8.GetString(buffer |> Array.take result.Count)
            match message |> Decode.fromString BaseMessage.Decoder with
            | Ok baseMessage ->
                printfn "%s %s" (baseMessage.messageType.ToString()) baseMessage.auth
                handlerTable
                |> Map.tryFind baseMessage.messageType
                |> Option.iter
                    (fun handler -> handler baseMessage.auth message socket)
            | _ ->
                ()
            keepServing buffer socket

    let internal serve(socket:WebSocket) : Tasks.Task =
        let buffer = [0..4095] |> List.map(fun _ -> 0uy) |> List.toArray
        keepServing buffer socket
